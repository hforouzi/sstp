#!/bin/bash

pip3 install sstp-server

echo "name sstpd
refuse-pap
refuse-chap
refuse-mschap
require-mschap-v2
nologfd
nodefaultroute
ms-dns 8.8.8.8
ms-dns 8.8.4.4" > /etc/ppp/options.sstpd

echo "hos * 123456 * " > /etc/ppp/chap-secrets

echo "[DEFAULT]
# 1 to 50. Default 20, debug 10, verbose 5
;log_level = 20

# OpenSSL cipher suite. See ciphers(1).
;cipher = EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH

# Path to pppd
;pppd = /usr/bin/pppd

# pppd config file path
;pppd_config = /etc/ppp/options.sstpd

# SSTP port
listen = 0.0.0.0
listen_port = 443

# PEM-format certificate with key.
pem_cert = /usr/local/local/cert.pem
pem_key = /usr/local/local/privkey.pem

# Address of server side on ppp.
local = 192.168.0.1

# If RADIUS is used to mangle IP pool, comment it out.
remote = 192.168.0.0/24" >  /etc/sstpd.ini
sysctl -w net.ipv4.ip_forward=1
iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -o eth0 -j MASQUERADE
iptables-save > /etc/iptables/rules.v4
echo "[Unit]
Description=sstpd
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/sstpd -f /etc/sstpd.ini
Restart=on-failure
RestartSec=30

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/sstpd.service
systemctl start sstpd.service
systemctl enable sstpd
